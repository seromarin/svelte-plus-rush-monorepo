/// <reference types="svelte" />
import { SvelteComponentTyped } from "svelte";

export interface ComponentProps {
  /**
   * User name
   * @default "Sebastian"
   */
  name?: String;

  /**
   * User last name
   * @default "Rodriguez"
   */
  lastName?: String;
}

export default class Component extends SvelteComponentTyped<
  ComponentProps,
  {},
  {}
> {}
